package com.example.shemajamebeliotxi

import android.os.Bundle
import android.telecom.Call
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shemajamebeliotxi.databinding.FragmentGameBinding


class gameFragment : Fragment() {
    val args: gameFragmentArgs by navArgs()
    private lateinit var adapter:gameAdapter
    lateinit var callBack: Call
    private  var binding: FragmentGameBinding? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentGameBinding.inflate(inflater,container, false)
        playGame()
        return binding!!.root

    }
    fun playGame()
    {

        adapter= gameAdapter(args.amount.toString())
        adapter.call={ item, x->
            item.setImageResource(x)
        }
        binding?.rvGame?.adapter=adapter
        binding?.rvGame?.layoutManager= GridLayoutManager(context, args.amount.toInt())

    }



}
