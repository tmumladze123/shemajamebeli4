package com.example.shemajamebeliotxi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.shemajamebeliotxi.databinding.FragmentEntryBinding


class entryFragment : Fragment() {
    private lateinit var binding: FragmentEntryBinding
    public var  gridList= mutableListOf<Int>(3,4,5)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         binding=FragmentEntryBinding.inflate(inflater,container, false)
        config()
        return binding.root
    }
    fun config() {
        binding.start.setOnClickListener {
                if (binding.amount.text.toString().toInt() in gridList) {
                   var action=entryFragmentDirections.actionEntryFragmentToGameFragment(binding.amount.text.toString().toInt())
                    findNavController().navigate(action)
                }
                else
                Toast.makeText(context, "please enter number 3,4,5", Toast.LENGTH_SHORT).show();
        }
    }

}