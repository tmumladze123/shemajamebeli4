package com.example.shemajamebeliotxi


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebeliotxi.databinding.ItemlayoutBinding

typealias callBack = (ImageButton, Int) -> Unit

class gameAdapter(var amount: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    public lateinit var call: callBack
    private val x: Int = R.drawable.x
    private val o: Int = R.drawable.o
    var start=true
    inner class ViewHolder(var binding: ItemlayoutBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        fun onBind() {
            var item = binding.ibitem
            binding.ibitem.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            if(start==true)
            {
                call.invoke(binding.ibitem, x)
                start=false
            }
            else
            {   call.invoke(binding.ibitem, o)
                start=true
            }
            
        }

    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.onBind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemlayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = amount.toInt() * amount.toInt()


}